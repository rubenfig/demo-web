import {apiClient} from './api.utils';
const path = 'cursos';

const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

export const CursosService = {
  listar,
};
