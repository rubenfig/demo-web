import {apiClient} from './api.utils';

const path = 'alumnos';
const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const agregarSaldo = async (idAlumno, saldo) => {
  const params = `?idAlumno=${idAlumno}&saldo=${saldo}`;
  return await apiClient.get(`${path}/cargarsaldo` + params);
};

const modificarAlumno = async usuario => {
  return await apiClient.post(`${path}/modificarAlumno`, usuario);
};

export const AlumnosService = {
  listar,
  agregarSaldo,
  modificarAlumno,
};
