import axios from 'axios';

export const url =
  '/webresources';
export const urlDownload =
  '/';

// export const url =
//   'http://app.helitactica-vuelos.com:23080/ServiVuelos-web/webresources';
// export const urlDownload =
//   'http://app.helitactica-vuelos.com:23080/ServiVuelos-web';

// export const url =
//   'http://app.helitactica-vuelos.com:23080/ServiVuelos-web/webresources';
// export const urlDownload =
//   'http://app.helitactica-vuelos.com:23080/ServiVuelos-web';
//
// export const url =
//   'http://www.helitactica-vuelos.com:36223/ServiVuelos-web/webresources';
// export const urlDownload =
//   'http://www.helitactica-vuelos.com:36223/ServiVuelos-web';

const apiClient = axios.create({
  baseURL: url,
  timeout: 5000,
  withCredentials: true,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});
apiClient.interceptors.response.use(
  function(response) {
    console.log(response?.request);
    return response;
  },
  function(error) {
    // console.log(error?.response?.request);
    if (
      error?.response?.status === 401 &&
      !error?.response?.request?._url?.includes('sesion') &&
      !error?.response?.request?._url?.includes('contacto')
    ) {
      console.log('error sesion');
      // NavigationRoot.replaceStack('Login');
    } else {
      return Promise.reject(error);
    }
  },
);
const apiDownloadClient = axios.create({
  baseURL: urlDownload,
  withCredentials: true,
  headers: {
    // crossDomain: true,
    Accept: 'application/json',
  },
});
export {apiClient, apiDownloadClient};
