import {apiClient} from './api.utils';
const path = 'noticia';

const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const modificar = async noticia => {
  return await apiClient.post(`${path}/modificar`, noticia);
};
const cargar = async noticia => {
  return await apiClient.get(`${path}/cargar`, noticia);
};
export const NoticiasService = {
  listar,
};
