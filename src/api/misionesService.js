import {apiClient} from './api.utils';
const path = 'misiones';

const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

export const MisionesService = {
  listar,
};
