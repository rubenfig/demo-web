import 'intl';
import 'intl/locale-data/jsonp/es';
export const reemplazarGuion = obj => {
  return typeof obj === 'string' ? obj.toString().replace(/-/g, '/') : obj;
};
export const isEmail = str => {
  return /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(str);
};
// eslint-disable-next-line no-undef
export const numberFormat = new Intl.NumberFormat('es-ES', {
  maximumSignificantDigits: 3,
});

export const menuPerfil = {
  reservas: {
    title: 'Reservas',
    url: '/reservas',
    icon: 'calendar',
  },
  reservas_instructor: {
    title: 'Reservas',
    url: '/reservas',
    icon: 'calendar',
  },
  alumnos: {
    title: 'Alumnos',
    url: 'AlumnosListado',
    icon: 'ios-contacts',
  },
  aeronaves: {
    title: 'Aeronaves',
    url: 'AeronavesListado',
    icon: 'ios-airplane',
  },
  instructores: {
    title: 'Instructores',
    url: 'InstructoresListado',
    icon: 'ios-people',
  },
  perfil: {
    title: 'Perfil',
    url: '/perfil',
    icon: 'ios-person',
  },
  atencion: {
    title: 'Atención',
    url: 'Atencion',
    icon: 'ios-list',
  },
  notificaciones: {
    title: 'Notificaciones',
    url: 'NotificacionesListado',
    icon: 'ios-notifications',
  },
  formularios: {
    title: 'Formularios',
    url: 'ReportesListado',
    icon: 'md-clipboard',
  },
  usuarios: {
    title: 'Usuarios',
    url: 'UsuariosListado',
    icon: 'ios-contacts',
  },
  configuraciones: {
    title: 'Configuraciones',
    url: '/configuraciones',
    icon: 'ios-settings',
  },
  aeronaves_0_hs: {
    title: 'Mantenimiento',
    url: 'Mantenimientos',
    icon: 'ios-cog',
  },
  aeronaves_secretaria: {
    title: 'Aeronaves',
    url: '/list',
    icon: 'ios-list',
  },
  aprobar_reservas: {
    title: 'Aprobar reservas',
    url: 'AprobarReservas',
    icon: 'ios-calendar',
  },
  pistas: {
    title: 'Pistas',
    url: 'PistasListado',
    icon: 'ios-remove-circle-outline',
  },
};

export const reservaColors = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#f1c40f',
    secondary: '#FDF1BA',
  },
};

export const reservaEstados = [
  {
    idEstadoReserva: 1,
    descripcion: 'PRERESERVA',
    color: {
      primary: '#f1c40f',
      secondary: '#FDF1BA',
    },
  },
  {
    idEstadoReserva: 6,
    descripcion: 'FINALIZADO',
    color: {
      primary: '#7044ff',
      secondary: '#633ce0',
    },
  },
  {
    idEstadoReserva: 3,
    descripcion: 'CANCELADO SECRETARIA',
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
  },
  {
    idEstadoReserva: 7,
    descripcion: 'CANCELADO SISTEMA',
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
  },
  {
    idEstadoReserva: 4,
    descripcion: 'CANCELADO TALLER',
    color: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
  },
  {
    idEstadoReserva: 5,
    descripcion: 'MANTENIMIENTO',
    color: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
  },
  {
    idEstadoReserva: 2,
    descripcion: 'RESERVADO',
    color: {
      primary: '#10dc60',
      secondary: '#0ec254',
    },
  },
];
