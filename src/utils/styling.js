import {create} from 'react-native-pixel-perfect';

const colors = {
  tint1: '#323B45',
  tint2: '#FF672B',
  tint3: '#323643',
  primary: '#F87E01',
  secondary: '#FF9933',
  gray: '#878787',
  black: '#000',
  white: '#fff',
  background: '#F9FAFB',
};

const perfectSize = create({
  width: 375,
  height: 812,
});

export function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 0.5 * elevation},
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation,
  };
}

export {colors, perfectSize};
