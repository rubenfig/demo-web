const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/webresources',
    createProxyMiddleware({
      target: 'http://app.helitactica-vuelos.com:23080/ServiVuelos-web',
      changeOrigin: true,
    })
  );
};
